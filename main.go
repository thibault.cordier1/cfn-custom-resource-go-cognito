package main

import (
	"context"
	"fmt"
	. "github.com/aws/aws-lambda-go/cfn"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
	"github.com/rs/xid"
	"log"
)

func fetchCloudFrontCname(cognitoUserPoolId string) (cloudfrontCname string, err error) {
	log.Println("Fetching information about CognitoUserPool id:", cognitoUserPoolId)
	sess, err := session.NewSession()

	if err != nil {
		fmt.Println(err)
		return "", err

	}
	svc := cognitoidentityprovider.New(sess)
	describeUserPoolInput := cognitoidentityprovider.DescribeUserPoolInput{
		UserPoolId: &cognitoUserPoolId,
	}

	describeUserPool, err := svc.DescribeUserPool(&describeUserPoolInput)
	if err != nil {
		fmt.Println(err)
		return "", err

	}
	domainName := describeUserPool.UserPool.CustomDomain
	describeUserPoolDomainInput := cognitoidentityprovider.DescribeUserPoolDomainInput{
		Domain: domainName,
	}
	describeDomain, err := svc.DescribeUserPoolDomain(&describeUserPoolDomainInput)
	if err != nil {
		fmt.Println(err)
		return "", err

	}

	cloudFrontDistribution := describeDomain.DomainDescription.CloudFrontDistribution

	return *cloudFrontDistribution, nil
}

func HandleRequest(ctx context.Context, event *Event) {
	log.Println("Response Type: ", event.RequestType)
	log.Println("Response Url: ", event.ResponseURL)
	log.Println("StackId: ", event.StackID)
	log.Println("RequestId: ", event.RequestID)
	log.Println("ResourceType: ", event.ResourceType)
	log.Println("LogicalResourceId: ", event.LogicalResourceID)
	log.Println("ResourceProperties.StackName: ", event.ResourceProperties)
	UserPoolId := ""
	for key, value := range event.ResourceProperties {
		if key == "CognitoUserPool" {
			log.Println(value)
			UserPoolId = fmt.Sprintf("%v", value)
		}

	}

	id := xid.New()
	responseData := make(map[string]interface{})
	status := StatusFailed
	reason := "Lambda failed"

	if event.RequestType == "Delete" {
		log.Println("Cloudformation delete")
		status = StatusSuccess
		reason = "Delete is always a success.."
	}

	if event.RequestType == "Create" || event.RequestType == "Update" {
		log.Println("Cloudformation action: ", event.RequestType)
		// Fetch requested informations
		cloudFrontCname, err := fetchCloudFrontCname(UserPoolId)
		if err != nil {
			fmt.Println(err)
			status = StatusFailed
		} else {
			status = StatusSuccess
			reason = "Create was successfull..."
			responseData["cloudfrontCname"] = cloudFrontCname
		}

	}

	response := NewResponse(event)
	response.PhysicalResourceID = event.LogicalResourceID + "-" + id.String()
	response.Reason = reason
	response.NoEcho = false
	response.Status = status
	response.Data = responseData

	err := response.Send()
	if err != nil {
		fmt.Println(err)
	}

}

func main() {
	lambda.Start(HandleRequest)
}
