.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Build binary
	@echo "Build main"
	@go build main.go

zip: ## Zip main.go to
	@zip lambda-cognito main

upload-lambda: ## Upload lambda to S3
	@aws s3 cp lambda-cognito.zip s3://devops-lambda-storage --profil default-perso

update-lambda: build zip upload-lambda ## Update Lambda from S3
	@aws lambda update-function-code --function-name AAAA --s3-bucket devops-lambda-storage --s3-key lambda-cognito.zip --publish --profil default-perso