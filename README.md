# CloudFormation custom Resource - POC Cognito

Invoke a Lambda function to fetch a non exposed Cognito User Pool custom domain Attribute with CloudFormation

The function is invoked with CognitoUserPoolId and will return the CNAME of the Cloudfront distribution. 

The Lambda function is written in Golang. 

Used to **demonstrate** the use of custom resources in CloudFormation

### Resources deployed:

- CognitoUserPool
- CognitoUserPoolDomain (custom domain)
- IAM Role for Lambda function
- IAM Policy for Lambda function
- Lambda function in Go 


#### Requirements: 

- Create Zone in Route53
- Create a corresponding **certificate in ACM** in region us-east-1 (Virginia)

 https://docs.aws.amazon.com/cognito/latest/developerguide/cognito-user-pools-add-custom-domain.html 


###Output
Cloudfront CNAME for HostedUI (te be use with Route53 for example)